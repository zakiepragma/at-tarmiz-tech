import Home from "./components/Home";
import Navbar from "./components/Navbar";

function App() {
  return (
    <div className="">
      <Navbar />
      <p className="">PT. AT-TARMIZ TEKNOLOGI, PEKANBARU</p>
      <Home />
    </div>
  );
}

export default App;
