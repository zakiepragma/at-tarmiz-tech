import React from 'react'
import HeroImage from '../assets/images/hero.png'
import {MdOutlineKeyboardArrowRight} from 'react-icons/md'

const Home = () => {
  return (
    <div name="home" className='h-screen w-full bg-gradient-to-b from-black to-gray-800'>
        <div className='max-w-screen-lg mx-auto flex flex-col justify-center items-center h-full px-4 md:flex-row'>
            <div className='flex flex-col justify-center h-full'>
                <h2 className='text-4xl sm:text-7xl font-bold text-white'>I'm Full Stack Developer</h2>
                <p className='text-gray-500 py-4'>
                I have almost 2 years working experience as programmer. Now, i really love the job of developing web-based applications using technologies like laravel, react, vue, talwind, bootstrap, mysql
                </p>
                <div>
                    <button className='group text-white w-fit px-6 py-3 my-2 flex items-centers rounded-md bg-gradient-to-r from-cyan-500 to-blue-500 cursor-pointer'>
                        Portofolio
                        <span className='group-hover:rotate-90 duration-300'>
                            <MdOutlineKeyboardArrowRight size={25} className="ml-1"/>
                        </span>
                    </button>
                </div>
            </div>
            <div>
                <img src={HeroImage} alt="hehe" className='rounded-2xl mx-auto md:w-full'/>
            </div>
        </div>
    </div>
  )
}

export default Home